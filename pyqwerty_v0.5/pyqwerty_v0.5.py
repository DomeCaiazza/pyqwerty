## AUTORE: Domenico Caiazza
## VERSIONE: 0.5
## INFO: https://domenicocaiazza.com/pyqwerty-v0-5-codifica-e-decodifica-di-un-testo-in-python/
import random
import argparse
import sys

def reverse(string): #capovolce la string
    reverse = str()
    for character in string:
        reverse = character + reverse
    return reverse

def split_chr(string): #porta in formato ord tutti i caratteri della string e li divide con un |
    split_chr = "|"
    for character in string:
        split_chr += str(ord(character)) + "|"
    return split_chr

def solution_split_chr(string): #trasforma tutti i valori separati con un | da ord in chr
    solution = str()
    for character in string.split("|"):
        if character != "":
            solution += str(chr(int(character)))
    return solution

rubrik_array = []


def rubrik_calc():
    ascii = ['!', 'Q', 'A', 'Z', '"', 'W', 'S', 'X', '£', 'E', 'D', 'C', '$', 'R',
        'F', 'V', '%', 'T', 'G', 'B', '&', 'Y', 'H', 'N', '/', 'U', 'J', 'M',
        '(', 'I', 'K', ';', ')', 'O', 'L', ':', '=', 'P', 'ç', '_', '?', '+', '°', '^', '*', '§']
    for _ in range(0,len(ascii)):
        character_scelto = random.choice(ascii)
        ascii.remove(character_scelto)
        rubrik_array.append(character_scelto)

def rubrik_string():
    rubrik_string = ""
    for rubrik_value in rubrik_array:
        rubrik_string += rubrik_value
    return rubrik_string



def mix(string,key): # ogni numero della string viene sostituito da uno dei 4 o 6 caratteri casuali indicati
    mixed_string = str()
    for character in string:
        if character == "0":
            mixed_string += (random.choice(key[0]+key[1]+key[2]+key[3]))
        elif character == "1":
            mixed_string += (random.choice(key[4]+key[5]+key[6]+key[7]))
        elif character == "2":
            mixed_string += (random.choice(key[8]+key[9]+key[10]+key[11]))
        elif character == "3":
            mixed_string += (random.choice(key[12]+key[13]+key[14]+key[15]))
        elif character == "4":
            mixed_string += (random.choice(key[16]+key[17]+key[18]+key[19]))
        elif character == "5":
            mixed_string += (random.choice(key[20]+key[21]+key[22]+key[23]))
        elif character == "6":
            mixed_string += (random.choice(key[24]+key[25]+key[26]+key[27]))
        elif character == "7":
            mixed_string += (random.choice(key[28]+key[29]+key[30]+key[31]))
        elif character == "8":
            mixed_string += (random.choice(key[32]+key[33]+key[34]+key[35]))
        elif character == "9":
            mixed_string += (random.choice(key[36]+key[37]+key[38]+key[39]))
        elif character == "|":                          #per il character | ci sono 6 possibili caratteri
            mixed_string += (random.choice(key[40]+key[41]+key[42]+key[43]+key[44]+key[45]))
    return mixed_string

def solution_mix(string, key): #riporta ogni possibile character al suo valore intero predestinato
    solution =str()
    for character in string:
        if character in key[0]+key[1]+key[2]+key[3]:
            solution += "0"
        elif character in key[4]+key[5]+key[6]+key[7]:
            solution += "1"
        elif character in key[8]+key[9]+key[10]+key[11]:
            solution += "2"
        elif character in key[12]+key[13]+key[14]+key[15]:
            solution += "3"
        elif character in key[16]+key[17]+key[18]+key[19]:
            solution += "4"
        elif character in key[20]+key[21]+key[22]+key[23]:
            solution += "5"
        elif character in key[24]+key[25]+key[26]+key[27]:
            solution += "6"
        elif character in key[28]+key[29]+key[30]+key[31]:
            solution += "7"
        elif character in key[32]+key[33]+key[34]+key[35]:
            solution += "8"
        elif character in key[36]+key[37]+key[38]+key[39]:
            solution += "9"
        elif character in key[40]+key[41]+key[42]+key[43]+key[44]+key[45]:
            solution += "|"
    return solution

def compile_string(string): #concatena le operazioni per la codifica di una string
    rubrik_array.clear()
    rubrik_calc()
    capovolgi = reverse(string)
    dividi = split_chr(capovolgi)
    mixa = mix(dividi,rubrik_array)
    return mixa, rubrik_string()

def decompile_string(string,key): #concatena le operazioni per la decodifica di una string
    solution_mixa = solution_mix(string,key)
    solution_dividi = solution_split_chr(solution_mixa)
    capovolgi = reverse(solution_dividi)
    return capovolgi

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
# --------------------------------------------------------------------
print("---------------------------------------------")
print("")
print("Welcome to PYQWERTY - Text Encoder")
print("")
print("Commands:")
print("encode\t\t-\tencode a string")
print("encode-file\t-\tencode a text file")
print("decode\t\t-\tdecode a string")
print("decode-file\t-\tdecode a text file")
print("help\t\t-\tsee all the commands")
print("exit\t\t-\texit from this program")

while 1:
    print("")
    text = input("Choose a command: ")

    if text =="exit":
        exit()
    if text =="encode":
        print("Type the text to code:")
        text = input("")
        print("")
        print("---------------------------------------------")
        print("ENCODED TEXT:")
        encoded_text = compile_string(text)
        print(encoded_text[0])
        print("")
        print("DECODING KEY:")
        print(encoded_text[1])
    if text =="decode":
        print("Type the text encoded:")
        encoded_text = input("")
        print("Type the decoding key:")
        decoding_key = input("")
        decoded_text = decompile_string(encoded_text,decoding_key)
        print("")
        print("---------------------------------------------")
        print("DECODED TEXT:")
        print(decoded_text)
    if text =="encode-file":
        print("")
        print("Type the PATH and name of the file:")
        print("eg. 'filename.txt' - 'C:/filename.txt' - '/home/user/filename.txt'")
        path = input("")
        with open(path, 'r') as reader:
            encode_file = compile_string(reader.read())
        with open(path + ".pyq", "w") as writer:
            writer.write(encode_file[0])

        print("---------------------------------------------")
        print("ENCODED FILE: " + path + ".pyq")
        print("")
        print("DECODING KEY:")
        print(encode_file[1])
        print("")
        generate_key_file = input("Generate key file?[y/n]: ")
        if generate_key_file.lower() == "y":
            with open(path + ".pyq.key", "w") as writer:
                writer.write(encode_file[1])
            print("---------------------------------------------")
            print("DECODING KEY: " + path + ".pyq.key")
        else:
            print("")
            print("Save somewhere the key or the file can't be decoded!")
            print(encode_file[1])
    if text =="decode-file":
        print("")
        print("Type the PATH and name of the file:")
        print("eg. 'filename.txt.pyq' - 'C:/filename.txt.pyq' - '/home/user/filename.txt.pyq'")
        path_encoded_file_name = input("")
        inline_key = input("Have an inline key?[y/n]: ")
        if inline_key.lower() == "y":
            key_file = input("Insert key: ")
        else:
            print("Type the PATH and name of key file:")
            print("eg. 'filename.txt.pyq.key' - 'C:/filename.txt.pyq.key' - '/home/user/filename.txt.pyq.key'")
            path_key = input("")
            with open(path_key, 'r') as reader:
                key_file = reader.read()

        with open(path_encoded_file_name, 'r') as reader:
            encoded_file = reader.read()
        with open(path_encoded_file_name + ".decoded", "w") as writer:
            writer.write(decompile_string(encoded_file,key_file))
    if text =="help":
        print("Commands:")
        print("encode\t\t-\tencode a string")
        print("encode-file\t-\tencode a text file")
        print("decode\t\t-\tdecode a string")
        print("decode-file\t-\tdecode a text file")
        print("help\t\t-\tsee all the commands")
        print("exit\t\t-\texit from this program")
