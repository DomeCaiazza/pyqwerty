## AUTORE: Domenico Caiazza
## VERSIONE: 0.1
## INFO: https://domenicocaiazza.com/pyqwerty-un-semplice-algoritmo-di-cifratura-in-python
import random
def reverse(stringa): #capovolce la stringa
    reverse = str()
    for carattere in stringa:
        reverse = carattere + reverse
    return reverse

def split_carattere(stringa): #porta in formato ord tutti i caratteri della stringa e li divide con un |
    split_carattere = "|"
    for carattere in stringa:
        split_carattere += str(ord(carattere)) + "|"
    return split_carattere

def soluzione_split_carattere(stringa): #trasforma tutti i valori separati con un | da ord in chr
    soluzione = str()
    for carattere in stringa.split("|"):
        if carattere != "":
            soluzione += str(chr(int(carattere)))
    return soluzione

def mix(stringa): # ogni numero della stringa viene sostituito da uno dei 4 o 6 caratteri casuali indicati
    stringa_mixata = str()
    for carattere in stringa:
        if carattere == "0":
            stringa_mixata += (random.choice("!QAZ"))
        elif carattere == "1":
            stringa_mixata += (random.choice('"WSX'))
        elif carattere == "2":
            stringa_mixata += (random.choice("£EDC"))
        elif carattere == "3":
            stringa_mixata += (random.choice("$RFV"))
        elif carattere == "4":
            stringa_mixata += (random.choice("%TGB"))
        elif carattere == "5":
            stringa_mixata += (random.choice("&YHN"))
        elif carattere == "6":
            stringa_mixata += (random.choice("/UJM"))
        elif carattere == "7":
            stringa_mixata += (random.choice("(IK;"))
        elif carattere == "8":
            stringa_mixata += (random.choice(")OL:"))
        elif carattere == "9":
            stringa_mixata += (random.choice("=Pç_"))
        elif carattere == "|":                          #per il carattere | ci sono 6 possibili caratteri
            stringa_mixata += (random.choice("?é°^*§"))
    return stringa_mixata

def soluzione_mix(stringa): #riporta ogni possibile carattere al suo valore intero predestinato
    soluzione =str()
    for carattere in stringa:
        if carattere in "!QAZ":
            soluzione += "0"
        elif carattere in '"WSX':
            soluzione += "1"
        elif carattere in "£EDC":
            soluzione += "2"
        elif carattere in "$RFV":
            soluzione += "3"
        elif carattere in "%TGB":
            soluzione += "4"
        elif carattere in "&YHN":
            soluzione += "5"
        elif carattere in "/UJM":
            soluzione += "6"
        elif carattere in "(IK;":
            soluzione += "7"
        elif carattere in ")OL:":
            soluzione += "8"
        elif carattere in "=Pç_":
            soluzione += "9"
        elif carattere in "?é°^*§":
            soluzione += "|"
    return soluzione

def cifra(stringa): #concatena le operazioni per la codifica di una stringa
    capovolgi = reverse(stringa)
    dividi = split_carattere(capovolgi)
    mixa = mix(dividi)
    return mixa

def decifra(stringa): #concatena le operazioni per la decodifica di una stringa
    soluzione_mixa = soluzione_mix(stringa)
    soluzione_dividi = soluzione_split_carattere(soluzione_mixa)
    capovolgi = reverse(soluzione_dividi)
    return capovolgi
