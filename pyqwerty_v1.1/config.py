## PYQWERTY v1.1 Config File
## AUTHOR: Domenico Caiazza
## MORE: https://domenicocaiazza.com/tag/pyqwerty/

# Characters used for text encoding
rubrik_array = ['m', 'Q', 'A', 'Z', 'n', 'W', 'S', 'X', 'b', 'E', 'D', 'C', 'v', 'R',
    'F', 'V', 'c', 'T', 'G', 'B', 'x', 'Y', 'H', 'N', 'z', 'U', 'J', 'M',
    'l', 'I', 'K', 'k', 'j', 'O', 'L', 'h', 'g', 'P', 'f', 'd', 's', 'a', 'p', 'o', 'i', 'u']

encoded_file_extension = ".pyq"
encoded_file_key_extension = ".k"
decoded_file_extension = ".pyq.de"
