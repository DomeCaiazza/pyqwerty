## PYQWERTY v1.1
## AUTHOR: Domenico Caiazza
## COLLABORATOR: Stefano Tafuto
## MORE: https://domenicocaiazza.com/tag/pyqwerty/
import random
import sys

try:
    import config
except:
    print("I need 'config.py' file.")
    print("Try it: wget https://raw.githubusercontent.com/DomeCaiazza/pyqwerty/master/pyqwerty_v1.1/config.py")
    exit()


def reverse(string):  #reverse a string
    return string[::-1]

def split_chr(string): #brings all the characters of the string in "ord" format and divides them with a |
    split_chr = "|"
    for character in string:
        split_chr += str(ord(character)) + "|"
    return split_chr

def split_chr_solution(string): #change all separate values (with a "|") from "ord" to "chr"
    solution = str()
    for character in string.split("|"):
        if character != "":
            solution += str(chr(int(character)))
    return solution

def rubrik(cmd): #
    if cmd == "new":    # generates a random rubrik_array
        random.shuffle(config.rubrik_array)
    elif cmd =="array": # returns rubrik_array array
        return config.rubrik_array
    elif cmd =="string": # returns rubrik_array string
        return ''.join(config.rubrik_array)

def mix(string,key): # each string number is replaced by one of the 4 or 6 random characters indicated
    mixed_string = str()
    for character in string:
        if character == "0":
            mixed_string += (random.choice(key[0]+key[1]+key[2]+key[3]))
        elif character == "1":
            mixed_string += (random.choice(key[4]+key[5]+key[6]+key[7]))
        elif character == "2":
            mixed_string += (random.choice(key[8]+key[9]+key[10]+key[11]))
        elif character == "3":
            mixed_string += (random.choice(key[12]+key[13]+key[14]+key[15]))
        elif character == "4":
            mixed_string += (random.choice(key[16]+key[17]+key[18]+key[19]))
        elif character == "5":
            mixed_string += (random.choice(key[20]+key[21]+key[22]+key[23]))
        elif character == "6":
            mixed_string += (random.choice(key[24]+key[25]+key[26]+key[27]))
        elif character == "7":
            mixed_string += (random.choice(key[28]+key[29]+key[30]+key[31]))
        elif character == "8":
            mixed_string += (random.choice(key[32]+key[33]+key[34]+key[35]))
        elif character == "9":
            mixed_string += (random.choice(key[36]+key[37]+key[38]+key[39]))
        elif character == "|":  # for the character | there are 6 possible characters
            mixed_string += (random.choice(key[40]+key[41]+key[42]+key[43]+key[44]+key[45]))
    return mixed_string

def mix_solution(string, key): #returns each possible character to its predetermined integer value
    solution =str()
    for character in string:
        if character in key[0]+key[1]+key[2]+key[3]:
            solution += "0"
        elif character in key[4]+key[5]+key[6]+key[7]:
            solution += "1"
        elif character in key[8]+key[9]+key[10]+key[11]:
            solution += "2"
        elif character in key[12]+key[13]+key[14]+key[15]:
            solution += "3"
        elif character in key[16]+key[17]+key[18]+key[19]:
            solution += "4"
        elif character in key[20]+key[21]+key[22]+key[23]:
            solution += "5"
        elif character in key[24]+key[25]+key[26]+key[27]:
            solution += "6"
        elif character in key[28]+key[29]+key[30]+key[31]:
            solution += "7"
        elif character in key[32]+key[33]+key[34]+key[35]:
            solution += "8"
        elif character in key[36]+key[37]+key[38]+key[39]:
            solution += "9"
        elif character in key[40]+key[41]+key[42]+key[43]+key[44]+key[45]:
            solution += "|"
    return solution

def encode(string,key="new_key"): #concatenates the operations for encoding a string
    try:
        if key == "new_key":
            rubrik("new")
        else:
            config.rubrik_array.clear()
            for value in key:
                config.rubrik_array.append(value)
        reverse_string = reverse(string)
        split_string = split_chr(reverse_string)
        mix_string = mix(split_string,config.rubrik_array)
        return mix_string, rubrik("string")
    except:
        print("Encoding Error")

def decode(string,key): #concatenates the operations for decoding a string
    try:
        mix_solution_string = mix_solution(string,key)
        solution_split = split_chr_solution(mix_solution_string)
        reverse_string = reverse(solution_split)
        return reverse_string
    except:
        print("Decoding Error")

def encode_file(path_and_file_name,inline_key="new_key",generate_key_file=1):
    try:
        if "." in path_and_file_name: # search dot for extension replace
            dot_index = path_and_file_name.rfind(".") #find last "." in the string
            new_path_and_file_name = path_and_file_name[0:dot_index] #file name without extension
        with open(path_and_file_name, 'r') as reader: # read original file
            encode_file = encode(reader.read(),inline_key)
        with open(new_path_and_file_name + config.encoded_file_extension, "w") as writer:
            writer.write(encode_file[0])
        if generate_key_file == 1:
            with open(new_path_and_file_name + config.encoded_file_key_extension, "w") as writer:
                writer.write(encode_file[1])
            return new_path_and_file_name + config.encoded_file_key_extension, encode_file[1]
        else:
            return encode_file[1] # returns the key to decode the file
    except:
        print("Encoding File Error")



def decode_file(path_encoded_file_name,path_and_key_file_name,key_file=0):
    try:
        key = str()
        if key_file == 0:
            key = path_and_key_file_name
        else:
            with open(path_and_key_file_name, 'r') as reader:
                key = reader.read()
        with open(path_encoded_file_name, 'r') as reader:
                encoded_file = reader.read()
        if "." in path_encoded_file_name:
            dot_index = path_encoded_file_name.rfind(".") #find last "." in the string
            path_encoded_file_name = path_encoded_file_name[0:dot_index] #file name without extension
        with open(path_encoded_file_name + config.decoded_file_extension, "w") as writer:
            text_decoded = decode(encoded_file,key)
            writer.write(text_decoded)
        return key, text_decoded
    except:
        return key,"Decoding File Error"


#------------------------------------------------------------ args

pyqwerty_args = sys.argv

pyqwerty_args.pop(0) #delete first argument (the name of the file)

TEXT = str()
KEY = str()
try:
    if "-" in pyqwerty_args[0]:
        operations = pyqwerty_args[0]
        operations = operations.replace("-","")
        if "h" in operations or operations =="help":  # show help
            print("PYQWERTY is a text encoder program.")
            print("")
            print("Examples:")
            print("   pyqwerty_vX -e \"text to encode\"\t\t\t\t# Encode a string")
            print("   pyqwerty_vX -ef file_to_encode.ext\t\t\t\t# Encode a file")
            print("   pyqwerty_vX -efk file_to_encode.ext \"KEY_FOR_ENCODE_FILE\"\t# Encode a file whit a specific inline key")
            print("")
            print("   pyqwerty_vX -d \"text to decode\" \"KEY_FOR_DECODE_FILE\"\t# Decode a string")
            print("   pyqwerty_vX -df file_to_decode.ext key_file.ext \t\t# Decode a file")
            print("   pyqwerty_vX -dfk file_to_decode.ext \"KEY_FOR_DECODE_FILE\" \t# Decode a file whit a specific inline key")
            print("")

            print("Options:")
            print("   -e\t# Encode")
            print("   -d\t# Decode")
            print("   -f\t# Encode or decode file.")
            print("   -n\t# Does not generate the key file. [only for \"d\" option]")
            print("   -k\t# Get an inline key for encode or decode file")
            print("   -x\t# Search extra options for file extensions")
            print("   -h\t# Show help")
            print("")
            print("Extra Options:")
            print("   --encoded_file_extension\t# Set encoded file extension  if \"x\" option is set")
            print("   --encoded_file_key_extension\t# Set encoded file key extension  if \"x\" option is set")
            print("   --decoded_file_extension\t# Set decoded file extension  if \"x\" option is set")

        if "x" in operations:
            if "--encoded_file_extension" in pyqwerty_args:
                command_index_0 = pyqwerty_args.index("--encoded_file_extension") + 1
                command_0 = pyqwerty_args[command_index_0]
                config.encoded_file_extension = command_0
            if "--encoded_file_key_extension" in pyqwerty_args:
                command_index_1 = pyqwerty_args.index("--encoded_file_key_extension") + 1
                command_1 = pyqwerty_args[command_index_1]
                config.encoded_file_key_extension = command_1
            if "--decoded_file_extension" in pyqwerty_args:
                command_index_2 = pyqwerty_args.index("--decoded_file_extension") + 1
                command_2 = pyqwerty_args[command_index_2]
                config.decoded_file_extension = command_2

        if "e" in operations and not "f" in operations and operations !="help": # encode argument
            command = pyqwerty_args[1]
            if "k" in operations: # Take a coding key from input
                command_2 = pyqwerty_args[2]
                exe = encode(command, command_2)
            else:
                exe = encode(command)
            TEXT = exe[0]
            KEY = exe[1]


        if "e" in operations and "f" in operations: # encode a file
            command = pyqwerty_args[1]
            key="new_key"
            if "k" in operations:
                key = pyqwerty_args[2]
            if "n" in operations: #not generate file key
                exe = encode_file(command,key,0)
                KEY = exe
            else:
                exe = encode_file(command,key,1)
                KEY = exe[0] + " > " + exe[1]
            TEXT = pyqwerty_args[1] + " file encoded."

        if "d" in operations and not "f" in operations:
            command = pyqwerty_args[1]
            command_2 = pyqwerty_args[2]
            exe = decode(command,command_2)
            TEXT = "\n" + exe
            KEY = pyqwerty_args[2]

        if "d" in operations and "f" in operations:
            if "k" in operations:
                command = pyqwerty_args[1]
                command_2 = pyqwerty_args[2]
                print(command_2)
                exe = decode_file(command,command_2,0)
                KEY = exe[0]
                TEXT = exe[1]
            else:
                command = pyqwerty_args[1]
                command_2 = pyqwerty_args[2]
                exe = decode_file(command,command_2,1)
                KEY = exe[0]
                TEXT = exe[1]
except:
    print("Error")

if TEXT != "":
    print("TEXT = ",TEXT)
if KEY != "":
    print("KEY = ",KEY)
